terraform {
  backend "s3" {
    bucket         = "bucket_name"
    key            = "openvpn/s3/terraform.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "terraform-state"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0.0"
    }
  }
}

provider "aws" {
  region = "eu-central-1"
}

variable "vpc_cidr_block" {}
variable "subnet_1_cidr_block" {}
variable "avail_zone" {}
variable "env_prefix" {}
variable "instance_type" {}
variable "ssh_key" {}
variable "ssh_private_key" {}
data "aws_ami" "amazon-linux-image" {

  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }
  owners = ["099720109477"]
}


output "ami_id" {
  value = data.aws_ami.amazon-linux-image.id
}

resource "aws_vpc" "openvpn-vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
    Name = "${var.env_prefix}-vpc"
  }
}

resource "aws_subnet" "openvpn-subnet-1" {
  vpc_id            = aws_vpc.openvpn-vpc.id
  cidr_block        = var.subnet_1_cidr_block
  availability_zone = var.avail_zone
  tags = {
    Name = "${var.env_prefix}-subnet-1"
  }
}

resource "aws_default_security_group" "openvpn-sg" {
  vpc_id = aws_vpc.openvpn-vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 1194
    to_port     = 1194
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 1194
    to_port     = 1194
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
    prefix_list_ids = []
  }

  tags = {
    Name = "${var.env_prefix}-security-group"
  }
}

resource "aws_internet_gateway" "openvpn-igw" {
  vpc_id = aws_vpc.openvpn-vpc.id

  tags = {
    Name = "${var.env_prefix}-internet-gateway"
  }
}

resource "aws_default_route_table" "openvpn-route-table" {
  default_route_table_id = aws_vpc.openvpn-vpc.main_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.openvpn-igw.id
  }

  tags = {
    Name = "${var.env_prefix}-route-table"
  }
}

resource "aws_route_table_association" "a-rtb-subnet" {
  subnet_id      = aws_subnet.openvpn-subnet-1.id
  route_table_id = aws_default_route_table.openvpn-route-table.id
}

resource "aws_key_pair" "ssh-key" {
  key_name   = "openvpn-key"
  public_key = var.ssh_key
}

resource "aws_instance" "openvpn-server" {
  ami                         = data.aws_ami.amazon-linux-image.id
  instance_type               = var.instance_type
  key_name                    = aws_key_pair.ssh-key.key_name
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.openvpn-subnet-1.id
  vpc_security_group_ids      = [aws_default_security_group.openvpn-sg.id]
  availability_zone           = var.avail_zone

  tags = {
    Name = "${var.env_prefix}-server"
  }

  provisioner "local-exec" {
    working_dir = "ansible"
    command     = "ansible-playbook -i ${aws_instance.openvpn-server.public_ip}, --private-key ${var.ssh_private_key} -u ubuntu install_vpn.yml"
  }

}

output "server-ip" {
  value = aws_instance.openvpn-server.public_ip
}
