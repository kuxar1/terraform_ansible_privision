### Deploy openvpn server by using terraform, ansilbe and openvpn installation script

To castomize instalation use vars files `terraform.vfvars` and `vars.yml` in ansille/vars/ directory.

Default settings in these files are:

**terraform.tfvars**:
```python
vpc_cidr_block      = "10.0.0.0/16"
subnet_1_cidr_block = "10.0.1.0/24"
avail_zone          = "eu-central-1a"
env_prefix          = "vpn"
instance_type       = "t2.micro"
ssh_key             = "ssh-ed25519 8xpk7ph8rfyHPq1G/NV7Vg"
ssh_private_key     = "~/.ssh/rsa"
```
**ansible/vars/vars.yml**:
```python
vpn_username: "user"
certificate_path: "/home/user/terraform/terraform_ansible_privision/"
```
By default use `Europe (Frankfurt)eu-central-1` region, as the closest locating to me.